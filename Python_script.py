import pyodbc
import pandas as pd
import numpy as np
import pyodbc
import translators as ts
import googletrans
import pycld2 as cld2
import regex

## Connect Azure SQL From Python
server = 'sst-intranet-srvr.database.windows.net'
database_name = 'sst-intranet-db'
username = 'vinothkumar.km@tvsmotor.com'
Authentication='ActiveDirectoryInteractive'
driver_nme='{ODBC Driver 18 for SQL Server}'
tcon = 'yes'

cnxn = pyodbc.connect(driver=driver_nme, host=server, database=database_name,
                      user=username, AUTHENTICATION=Authentication)

print(cnxn)

# Create a cursor from the connection
cursor = cnxn.cursor()

sql_query = '''SELECT A.Id,
A.InfraStructID,
A.CreatedBy,
A.CreatedUser,
A.BriefDescription,
A.CreatedDate,
A.ModifiedBy,
A.vdo_id,
A.vdo_name,
A.STATUS,
A.is_core_village,
A.ClusterName  
FROM dbo.Infra_VIEW_FOR_V2Visit A 
WHERE YEAR(A.CreatedDate)=2021 AND MONTH(A.CreatedDate)=10'''
df = pd.read_sql_query(sql_query, con=cnxn)
df

## Language Detection
df['BriefDescription'] = df['BriefDescription'].astype(str)
# df['BriefDescription_mod'] = df['BriefDescription'].str.encode('utf-8', 'ignore').str.decode('utf-8')
df["BriefDescription_mod"] = df['BriefDescription'].str.replace('[^\w\s]','')
df['Lang_output'] = df['BriefDescription_mod'].apply(lambda x: [r[0] for r in cld2.detect(x)[2]])
df['Lang_output'] = df['Lang_output'].astype(str)
df['Language'] = pd.np.where(df.Lang_output.str.contains("TAMIL"), "TAMIL",
                             pd.np.where(df.Lang_output.str.contains("KANNADA"), "KANNADA",
                                         pd.np.where(df.Lang_output.str.contains("TELUGU"), "TELUGU",
                                                     pd.np.where(df.Lang_output.str.contains("HINDI"), "HINDI",
                                                                 pd.np.where(df.Lang_output.str.contains("MARATHI"), "MARATHI",
                                                                             pd.np.where(df.Lang_output.str.contains("ENGLISH"), "ENGLISH","OTHERS"))))))

df_n_rows = df[0:30]

## Optimized if-else to take care multiple language inputs using lambda function
df_n_rows['Translated_text'] = df_n_rows[['BriefDescription','Language']].apply(lambda x: ts.bing(x['BriefDescription'], from_language='ta',to_language='en') if x['Language']=='TAMIL' 
                                                                               else ts.bing(x['BriefDescription'], from_language='kn',to_language='en') if x['Language']=='KANNADA' 
                                                                                else ts.bing(x['BriefDescription'], from_language='te',to_language='en') if x['Language']=='TELUGU' 
                                                                                else ts.bing(x['BriefDescription'], from_language='hi',to_language='en') if x['Language']=='HINDI' 
                                                                                else ts.bing(x['BriefDescription'], from_language='mr',to_language='en') if x['Language']=='MARATHI'
                                                                                else x['BriefDescription'],axis=1)

## Read keywords
dictionary = pd.read_csv(r'Z:\Downloads\text_analytics_keywords.csv')
dictionary

## Conver columns into list
action_req_list = dictionary['keywords_action_req'].to_list()
pattern_action_req = '|'.join(action_req_list)

positive_list = dictionary['keywords_positive'].to_list()
pattern_positive = '|'.join(positive_list)

## Search string and then tag it
df_n_rows['segments'] = np.where(df_n_rows.BriefDescription_mod.str.contains(pattern_action_req), "Action_Required",
                                 df_n_rows.BriefDescription_mod.str.contains(pattern_positive), "Positive","Neutral")

## Check segment count
df_n_rows['segments'].value_counts()

# Insert Dataframe into SQL Server:
for index, row in df.iterrows():
     cursor.execute("INSERT INTO dbo.XYZ(DepartmentID,Name,GroupName) values(?,?,?)", row.DepartmentID, row.Name, row.GroupName)
cnxn.commit()
cursor.close()



