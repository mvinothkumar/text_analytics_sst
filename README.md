This is created especially for SST to automate Text Analytics ML Model. 
Need to try various algorithms to achieve following task.

	a) Language Detection
	b) Translation
	c) Segmentation

How to use it:

Enter the required input when prompt. 

Dependencies:
As the program automatically read and write directly from/to Azure database, user login need to be specified. Do not share password with any one.

Required python packages:
pyodbc
googletranslate
translator
spacy

Pre-requisite:
Make sure whether you able to connect Azure from python. SST Azure login type is MFA.


